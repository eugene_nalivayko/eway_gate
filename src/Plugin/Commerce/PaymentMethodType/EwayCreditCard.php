<?php

namespace Drupal\eway_gate\Plugin\Commerce\PaymentMethodType;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "eway_credit_card",
 *   label = @Translation("eWay Credit card"),
 * )
 */
class EwayCreditCard extends CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return parent::buildLabel($payment_method);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['card_name'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Cardholder name'))
      ->setDescription(t('The credit card name.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
